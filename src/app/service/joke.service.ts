import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class JokeService {

  apiURL='https://jokeapi-v2.p.rapidapi.com/joke/Any';
  /* apiKey='e7f9bb50bemshd45ec84348544a5p183097jsnb8ab28b03b66';
  apiHost='jokeapi-v2.p.rapidapi.com'; */

  headerItems={
    'X-RapidAPI-Host': 'jokeapi-v2.p.rapidapi.com',
    'X-RapidAPI-Key': 'e7f9bb50bemshd45ec84348544a5p183097jsnb8ab28b03b66'
  }

  headerParam = {
    headers: new HttpHeaders(this.headerItems)
  }

  constructor(
    private httpClient: HttpClient

  ) { }

  getJoke1(){
    return this.httpClient.get(this.apiURL, this.headerParam);
  }
}
