import { JokeService } from './../../service/joke.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  content = 'you have been warned';
  hideContent = false;
  severity = 423;
  varNothing: any;
  joke: any;
  jokejson: any;

  constructor(
    private jokeService: JokeService
  ) { }

  ngOnInit(): void {
    /* this.getJoke(); */
  }

  toggle(){
    this.hideContent = !this.hideContent;
    if (this.hideContent) {
      this.jokeService.getJoke1().subscribe((data: any) => {
        this.jokejson = data;
        this.joke = data['joke' as any ] ? data['joke' as any]: data['setup' as any];
        console.log(this.joke);
        console.log(this.jokejson)
      })
    }
    console.log(this.hideContent)
  }
}
